//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Gareth on 02/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.hpp"
#include <iostream>
#include <cmath>
MidiMessage::MidiMessage(int initialNoteNumber, int initialVelocity, int initialChannel)//Constructor
{
    number = initialNoteNumber;
    velocity = initialVelocity;
    channel = initialChannel;
    std::cout << "Constructor\n";
}
MidiMessage::MidiMessage()//Constructor
{
    number = 60;
    velocity = 100;
    channel = 1;
    std::cout << "Constructor\n";
}
MidiMessage::~MidiMessage()//Destructor
{
    std::cout << "Destructor\n";
}

void MidiMessage::setNoteNumber (int value) //Mutator
{
    if (value < 128 && value > 0)
        number = value;
    
    return;
}
int MidiMessage::getNoteNumber() const //Accessor
{
    return number;
}
float MidiMessage::getMidiNoteInHertz() const //Accessor
{
    return 440 * pow(2, (number - 69) / 12.0);
}
float MidiMessage::getVelocityAmplitude() const //Accessor
{
    return velocity / 127.0;
}

