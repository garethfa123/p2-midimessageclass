//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
#include <cmath>
#include "MidiMessage.hpp"


int main()
{
    MidiMessage note (0,0,0);
    int setNum;

    std::cout << note.getNoteNumber() << " = Note Number\n";
    std::cout << note.getMidiNoteInHertz() << " = Note frequency\n";
    std::cout << note.getVelocityAmplitude() << " = Note Amplitude\n";
    std::cin >> setNum;
    note.setNoteNumber(setNum);
    std::cout << note.getNoteNumber() << " = Note number\n";
    std::cout << note.getMidiNoteInHertz() << " = Note frequency\n";
    
    return 0;
}
