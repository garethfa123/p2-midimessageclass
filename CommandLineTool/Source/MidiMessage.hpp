//
//  MidiMessage.hpp
//  CommandLineTool
//
//  Created by Gareth on 02/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#ifndef MidiMessage_hpp
#define MidiMessage_hpp
#include <stdio.h>

class MidiMessage
{
public:
    MidiMessage(int initialNoteNumber, int initialVelocity, int initialChannel);//Constructor
    
    MidiMessage();//Constructor
    
    ~MidiMessage();//Destructor
    
    
    void setNoteNumber (int value); //Mutator
    
    int getNoteNumber() const; //Accessor
    
    float getMidiNoteInHertz() const; //Accessor
    
    float getVelocityAmplitude() const; //Accessor
    
private:
    int number;
    int velocity;
    int channel;
};



#endif /* MidiMessage_hpp */
